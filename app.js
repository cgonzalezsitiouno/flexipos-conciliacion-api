require('./db/db_flexiProfiles.js');
require('./db/db_flexiWeb_Produccion.js');

const express = require('express'),
  app = express(),
  router = require('./routes.js'),
  bodyParser = require('body-parser');

app.set('port', 7000);

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));


app.use(function(req, res, next) {
  console.log("Getting request", req.method, req.url, req.body || 0);
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, content-type, Accept, Authorization");
  next();
});

app.use('/v1', router);

app.listen(app.get('port'), function() {
  console.log(`app listening on port ${app.get('port')}` );
});