'use strict';
var mongoose = require('mongoose'),
  { Schema } = mongoose;

var results = new Schema({
  date: {
    type: Number,
    unique: true
  },
  results: {
    type: [Number],
    required: true
  },
  stars: {
    type: [Number],
    required: true
  }
});                                  
mongoose.model("results", results);