var mongoose = require('mongoose'),
  {Schema} = mongoose;

var Terminales = new Schema({}, {collection: "Terminales", strict: false}),
  Transacciones = new Schema({}, {collection: "Transacciones", strict: false}),
  Lote = new Schema({}, {collection: "Lote", strict: false}),
  Usuarios = new Schema({}, {collection: "Usuarios", strict: false});

module.exports = {
  Terminales,
  Transacciones,
  Lote,
  Usuarios
};
