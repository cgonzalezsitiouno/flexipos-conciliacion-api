var mongoose = require('mongoose'),
  { Schema } = mongoose;

var AssignedSchema = new Schema({
    name: {
      type: String,
      requided: true
    },
    bankAccount: {
      type: String,
      required: true
    },
    dateAssignation: {
      type: Number,
      default: Date.now
    }
  }),
  TerminalSchema = new Schema({
    afiliado: {
      type: String,
      required: true
    },
    serial: {
      type: String,
      required: true
    },
    terminals: {
      type: String,
      required: true
    },
    locationTerm: {
      type: {},
      default: {}
    },
    assignedTo: {
      type: AssignedSchema,
      default: null
    },
    adminPercent: {
      type: Number,
      default: 0,
      max: 100,
      min: 0
    },
    userPercent: {
      type: Number,
      default: 0,
      max: 100,
      min: 0
    }
  }),
  Profile = new Schema({
    name: {
      type: String,
      required: true
    },
    terminals: {
      type: [TerminalSchema],
      default: []
    },
    created: {
      type: Number,
      default: Date.now
    }
  }, {collection: "Profiles", strict: false});


module.exports = Profile;