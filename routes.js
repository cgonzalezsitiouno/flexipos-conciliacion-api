const express = require('express'),
  mainCtrl = require('./controllers/main.ctrl.js'),
  router = express.Router();

router.route('/test').get(mainCtrl.test);

router.route('/profile')
  .get(mainCtrl.getProfiles)
  .post(mainCtrl.createProfile)
  .delete(mainCtrl.deleteProfiles);

router.route('/profile/:id')
  .get(mainCtrl.getProfileById)
  .put(mainCtrl.updateProfile);

router.route('/profile/:profileId/terminal/:terminalId')
  .put(mainCtrl.updateTerminal);

router.route('/terminal/:id/lotes')
  .post(mainCtrl.getLotesByTerminal);

router.route('/affiliate/:afiliado/terminals')
  .get(mainCtrl.getTerminalsByAfiliate);
module.exports = router;