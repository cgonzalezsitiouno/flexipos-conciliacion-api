#!/bin/bash

target_branch="master"
working_tree="/SU_services/conciliacion"
#zipfile="ws_web.zip"

while read oldrev newrev refname
do
    branch=$(git rev-parse --symbolic --abbrev-ref $refname)
    if [ -n "$branch" ] && [ "$target_branch" == "$branch" ]; then

       GIT_WORK_TREE=$working_tree git checkout $target_branch -f
       NOW=$(date +"%Y%m%d-%H%M")
       git tag release_$NOW $target_branch

       echo "   /==============================="
       echo "   | DEPLOYMENT COMPLETED"
       echo "   | Target branch: $target_branch"
       echo "   | Target folder: $working_tree"
       echo "   | Tag name     : release_$NOW"
       echo "   \=============================="
    fi
done

cd $working_tree
echo " /===============> NPM install starting"
sudo npm install
echo " /===============> NPM install finished"

# echo " /===============> ZIP starting"
# zip -r $zipfile *
# echo " /===============> ZIP finished"

pm2 restart 0
echo "Procces finished"

# echo " /====> Starting sftp"
# sftp 10.138.0.21 << SFTP

# cd /SU_services

# put  /SU_services/ws_web/$zipfile

# echo "Finished"
# echo " /====> sftp finished"

# quit
# SFTP

# echo " /====> Starting ssh"
# ssh 10.138.0.21 << SSH

# cd /SU_services
# export GLOBIGNORE=*.zip
# rm -r *
# echo " /===== Unzipping"
# unzip $zipfile
# export GLOBIGNORE=*.doc
# rm *.zip
# sudo pm2 restart 0

# exit

# SSH


# echo " /====== Finished"
