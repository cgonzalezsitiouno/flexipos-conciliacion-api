var mongoose = require('mongoose'),
  Profile = require('../models/conciliacion/profile.model.js'),
  dbBuilder = require('./connectionDb.js'),
  db = dbBuilder.db("conciliacion");

db.catch(err => console.log("Error en la conexion", err));
db.model("Profiles", Profile);

// Exportamos la conexion a conciliacion
module.exports = db;