var mongoose = require('mongoose');

module.exports = { db };

function db (dbName) {
  let conexStrings = {
    conciliacion: "mongodb://10.128.0.2:10180,10.128.0.3:10180,10.128.0.4:10180/conciliacion?replicaSet=prod-fpw-rs",
    flexiweb: "mongodb://10.128.0.2:10180,10.128.0.3:10180,10.128.0.4:10180/FlexiWeb_Produccion?replicaSet=prod-fpw-rs"
  }

  return mongoose.createConnection(conexStrings[dbName] || conexStrings[conciliacion], {
    user: "backup",
    pass: "$itio.{01}"
  });
}