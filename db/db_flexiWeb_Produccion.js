var mongoose = require('mongoose'),
  { Terminales, Transacciones, Lote, Usuarios } = require('../models/flexiWeb/models.js'),
  dbBuilder = require('./connectionDb.js'),
  db = dbBuilder.db("flexiweb");

db.catch(err => console.log("Error en la conexion", err));
db.model("Terminales", Terminales);
db.model("Transacciones", Transacciones);
db.model("Usuarios", Usuarios);
db.model("Lote", Lote);

// Exportamos la conexion a flexiweb
module.exports = db;