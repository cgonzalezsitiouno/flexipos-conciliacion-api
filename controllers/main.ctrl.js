const flexiWeb = require('../db/db_flexiWeb_Produccion.js'),
  conciliacion = require('../db/db_flexiProfiles.js'),
  mongoose = require('mongoose'),
  Profiles = conciliacion.model('Profiles'),
  Lotes = flexiWeb.model('Lote'),
  Transacciones = flexiWeb.model('Transacciones'),
  Terminales = flexiWeb.model('Terminales'),
  Usuarios = flexiWeb.model('Usuarios'),
  { _handleError, _handleQuery } = require('../globals.js');

  
module.exports = {
  test,
  createProfile,
  getProfiles,
  getProfileById,
  getTerminalsByAfiliate,
  updateProfile,
  deleteProfiles,
  updateTerminal,
  getLotesByTerminal
}

function test(req, res) {

  let codes = {
    Profiles: Profiles,
    Lotes: Lotes,
    Transacciones: Transacciones,
    Terminales: Terminales,
    Usuarios: Usuarios
  }, { code } = req.query;

  _prGet(code ? codes[code] : Lotes)()
    .then(r => res.status(200).jsonp({message: r}))
    .catch(e => res.status(400).jsonp({message: e}));

}

function getProfileById(req, res) {
  let { id } = req.params;

  _prGet(Profiles)({_id: id})
    .then(result => {
      res.status(200).jsonp({message: result})
    })
    .catch(err => _handleError(req, res, err));
}

function getProfiles(req, res) {
  _prGet(Profiles)()
    .then(result => {
      res.status(200).jsonp({message: result})
    })
    .catch(err => _handleError(req, res, err));
}

function createProfile (req, res) {
  let { Profile } = req.body;

  Profiles.create({
    name: Profile.name,
    terminals: Profile.terminals
  }, function(err, result) {
    if(err)
      return _handleError(req, res, err);

    return res.status(200).jsonp({message: result});
  });
}

function updateProfile(req, res) {
  let { id } = req.params;

  _prGet(Profiles)({_id: id})
    .then(([profile]) => {
      console.log("Este es el perfil para actualizar", profile);
      return res.status(201).jsonp({message: profile});
    })
    .catch(e => _handleError(req, res, err));
}

function getTerminalsByAfiliate (req, res) {
  let {afiliado} = req.params;

  _prGet(Terminales)({afiliado})
    .then(result => {
      res.status(200).jsonp({message: result})
    })
    .catch(err => _handleError(req, res, err));
}

function deleteProfiles (req, res) {
  Profiles.remove({}, function(err, result) {
    if(err) 
      return _handleError(req, res, err);

    return res.status(200).jsonp({message: result});
  });
}

function updateTerminal(req, res) {
  let { profileId, terminalId } = req.params,
    { terminal: newTerminal } = req.body;

  _prGet(Profiles)({_id: profileId})
    .then(([profile]) => {
      let _aux = profile.terminals.id(terminalId),
        { adminPercent=0, userPercent=0 } = newTerminal;
        if (adminPercent + userPercent < 101) {
          _aux.adminPercent = adminPercent;
          _aux.userPercent = userPercent;
          return profile.save(err => {
            if(err) {
              return _handleError(req, res, err);
            }
            return res.status(201).jsonp({message: _aux});
          });
        } 
        return res.status(400).jsonp({message: "Porcentajes no permitidos."});
    })
    .catch(e => _handleError(req, res, e));
}

function getLotesByTerminal(req, res) {
  let { terminal } = req.body,
    { id } = req.params,
    query = {};

  if(terminal && terminal.assignedTo && terminal.assignedTo.dateAssignation) {
    // let timeAssignation = terminal.assignedTo.dateAssignation / 1000;
    query = {
      // _id: {$gte: mongoose.Types.ObjectId(timeAssignation)}
      _id: {$gte: terminal.assignedTo.dateAssignation}
    }
  }
  query["terminal"] = terminal.terminals;
  query["afiliado"] = terminal.afiliado;
  query["serial"] = terminal.serial;
  query["estatus"] = true;
  
  _prGet(Lotes)(query)
    .then(message => res.status(200).jsonp({message}))
    .catch(err => _handleError(req, res, err));

}

function _prGet(_Schema) {
  return function(query, projection) {
    return new Promise(function(resolve, reject) {
      if (!_Schema || !_Schema.find)
        return reject("_Schema is not defined");
        
      _Schema
        .find(query, projection)
        .exec(function(err, result){
          if (err) {
            console.log("error en la promesa", err);
            return reject (err);
          }
          console.log('resolvi la promesa', result.length);
          return resolve(result);
        });
    });
  }
}