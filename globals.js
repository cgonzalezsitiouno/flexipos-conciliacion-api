
var LocalDic = {
  Enero: "Jan",
  Febrero: "Feb",
  Marzo: "Mar",
  Abril: "Apr",
  Mayo: "May",
  Junio: "Jun",
  Julio: "Jul",
  Agosto: "Aug",
  Septiembre: "Sep",
  Octubre: "Oct",
  Noviembre: "Nov",
  Diciembre: "Dic"
};

function customFilter($, className) {
  return function (index, data) {
    return $(data).hasClass(className);
  }
}

function mapFechas(fechas) {
  return (
    fechas
      .map(e => e.split("Sorteo del ")[1])
      .map(e => e.split(" "))
      .map(([tag,d,,m,,a]) => [d,m,a].join("-"))
      .map(e => new Date(e))
  );
}

function _handleError(req, res, err) {
  console.log("error ", err);
  res.status(500).send(err);
}

function _handleQuery(req, res) {
  return function(err, result) {
    if (err) {
      return _handleError(req, res, err);
    }
    return res.status(200).jsonp({message: result});
  }
}

module.exports = {
  _handleError,
  _handleQuery,
  mapFechas,
  customFilter,
  LocalDic
}